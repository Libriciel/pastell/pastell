<?php

class AttrFactureCPP
{
    public const ATTR_ID_FACTURE_CPP = 'id_facture_cpp';
    public const ATTR_STATUT_CPP = 'statut_cpp';
    public const ATTR_STATUT_CIBLE_LISTE = 'statut_cible_liste';
    public const ATTR_DESTINATAIRE = 'destinataire';
    public const ATTR_SIRET = 'siret';
    public const ATTR_SERVICE_DESTINATAIRE = 'service_destinataire';
    public const ATTR_SERVICE_DESTINATAIRE_CODE = 'service_destinataire_code';
    public const ATTR_FACTURE_NUMERO_ENGAGEMENT = 'facture_numero_engagement';
    public const ATTR_FACTURE_NUMERO_MARCCHE = 'facture_numero_marche';
    public const ATTR_DATE_MISE_A_DISPO = 'date_mise_a_dispo';
    public const ATTR_DATE_FIN_SUSPENSION = 'date_fin_suspension';
    public const ATTR_DATE_PASSAGE_STATUT = 'date_passage_statut';
    public const ATTR_FOURNISSEUR = 'fournisseur';
    public const ATTR_TYPE_FACTURE = 'type_facture';
    public const ATTR_NO_FACTURE = 'no_facture';
    public const ATTR_DATE_FACTURE = 'date_facture';
    public const ATTR_DATE_DEPOT = 'date_depot';
    public const ATTR_DATE_STATUT_COURANT = 'date_statut_courant';
    public const ATTR_MONTANT_TTC = 'montant_ttc';
    public const ATTR_FICHIER_FACTURE = 'fichier_facture';
    public const ATTR_MOTIF_MAJ = 'motif_maj';
    public const ATTR_TYPE_IDENTIFIANT = 'type_identifiant';
    public const ATTR_FOURNISSEUR_RAISON_SOCIALE = 'fournisseur_raison_sociale';
    public const ATTR_IS_CPP = 'is_cpp';
    public const ATTR_IS_ANNULE = 'is_annule';
    public const ATTR_NUMERO_MANDAT = 'numero_mandat';
    public const ATTR_TYPE_INTEGRATION = 'type_integration';
}
