nom: 'Récupération parapheur'
type: 'Documents divers'
description: 'Type de dossier permettant la connexion à un parapheur pour récupérer les dossiers'
connecteur:
    - transformation
    - GED
    - 'Bordereau SEDA'
    - SAE
affiche_one: false
formulaire:
    Information:
        dossier_id:
            name: 'Identifiant du dossier sur le parapheur'
            type: text
            requis: true
            multiple: false
            commentaire: ''
            index: true
        dossier_name:
            name: 'Nom du dossier'
            type: text
            requis: true
            multiple: false
            commentaire: ''
            title: true
        document_signe:
            name: 'Document signé'
            type: file
            requis: true
            multiple: true
            commentaire: ''
        annexe:
            name: Annexe
            type: file
            requis: false
            multiple: true
            commentaire: ''
        bordereau:
            name: Bordereau
            type: file
            requis: true
            multiple: false
            commentaire: ''
        premis:
            name: 'Fichier PREMIS'
            type: file
            requis: true
            multiple: false
            commentaire: "Le fichier PREMIS contient les métadonnées et l'historique du parapheur"
    Cheminement:
        envoi_transformation:
            name: Transformation
            type: checkbox
            onchange: cheminement-change
            default: checked
            read-only: true
        envoi_depot:
            name: 'Dépôt (GED, FTP, ...)'
            type: checkbox
            onchange: cheminement-change
            default: ''
            read-only: false
        envoi_sae:
            name: "Système d'archivage électronique"
            type: checkbox
            onchange: cheminement-change
            default: ''
            read-only: false
    Transformation:
        has_transformation:
            no-show: true
            read-only: true
        transformation_file:
            name: Transformations
            type: file
            visionneuse: Pastell\Viewer\JsonViewer
            read-only: true
            visionneuse-no-link: true
    'Retour GED':
        has_ged_document_id:
            no-show: true
            read-only: true
        ged_document_id_file:
            name: 'Identifiants des documents sur la GED'
            type: file
            visionneuse: Pastell\Viewer\GedIdDocumentsViewer
            read-only: true
            requis: true
            visionneuse-no-link: true
    SAE:
        sae_show:
            no-show: true
        journal:
            name: 'Faisceau de preuves'
            type: file
        date_journal_debut:
            name: 'Date du premier évènement journalisé'
        date_cloture_journal:
            name: 'Date de clôture du journal'
        date_cloture_journal_iso8601:
            name: 'Date de clôture du journal (iso 8601)'
        sae_transfert_id:
            name: 'Identifiant de transfert'
            index: true
        sae_bordereau:
            name: 'Bordereau SEDA'
            type: file
        sae_archive:
            name: "Paquet d'archive (SIP)"
            type: file
        ar_sae:
            type: file
            name: 'Accusé de réception SAE'
        sae_ack_comment:
            name: "Commentaire de l'accusé de réception"
        reply_sae:
            type: file
            name: 'Réponse du SAE'
        sae_archival_identifier:
            name: "Identifiant de l'archive sur le SAE"
        sae_atr_comment:
            name: 'Commentaire de la réponse du SAE'
champs-affiches:
    - titre
    - dernier_etat
    - date_dernier_etat
    - dossier_id
champs-recherche-avancee:
    - type
    - id_e
    - lastetat
    - last_state_begin
    - etatTransit
    - state_begin
    - notEtatTransit
    - search
    - dossier_id
page-condition:
    Transformation:
        has_transformation: true
    'Retour GED':
        has_ged_document_id: true
    SAE:
        sae_show: true
action:
    creation:
        name-action: Créer
        name: Créé
        rule:
            no-last-action: ''
    modification:
        name-action: Modifier
        name: 'En cours de rédaction'
        rule:
            last-action:
                - creation
                - modification
                - importation
                - transformation
                - send-ged
    supression:
        name-action: Supprimer
        name: Supprimé
        rule:
            last-action:
                - creation
                - modification
                - importation
                - termine
                - fatal-error
                - transformation-error
                - error-ged
                - rejet-sae
        action-class: Supprimer
        warning: 'Êtes-vous sûr ?'
    importation:
        name: 'Importation du document'
        rule:
            role_id_e: no-role
        action-automatique: orientation
    orientation:
        name: 'Envoi du document'
        name-action: 'Envoyer le document'
        rule:
            last-action:
                - creation
                - modification
                - importation
                - transformation
                - send-ged
                - accepter-sae
            document_is_valide: true
        action-class: OrientationTypeDossierPersonnalise
    termine:
        name: 'Traitement terminé'
        rule:
            role_id_e: no-role
    reouverture:
        name: 'Rouvrir le dossier'
        rule:
            last-action:
                - termine
        action-class: Reopen
    cheminement-change:
        no-workflow: true
        rule:
            role_id_e: 'no-role,'
        action-class: CheminementChangeTypeDossierPersonnalise
    preparation-transformation:
        name: 'Préparation à la transformation'
        rule:
            role_id_e: no-role
        action-automatique: transformation
    transformation:
        name-action: 'Transformer le dossier'
        name: 'Transformation du dossier'
        rule:
            last-action:
                - preparation-transformation
                - transformation-error
        action-class: StandardAction
        action-automatique: orientation
        connecteur-type: transformation
        connecteur-type-action: TransformationTransform
        connecteur-type-mapping:
            transformation_file: transformation_file
            has_transformation: has_transformation
        editable-content:
            - envoi_depot
            - envoi_sae
            - sae_show
            - journal
            - date_journal_debut
            - date_cloture_journal
            - date_cloture_journal_iso8601
            - sae_transfert_id
            - sae_bordereau
            - sae_archive
            - ar_sae
            - sae_ack_comment
            - reply_sae
            - sae_archival_identifier
            - sae_atr_comment
        modification-no-change-etat: true
    transformation-error:
        name: 'Erreur lors de la transformation du dossier'
        rule:
            role_id_e: no-role
    preparation-send-ged:
        name: "Préparation de l'envoi à la GED"
        rule:
            role_id_e: no-role
        action-automatique: send-ged
    send-ged:
        name-action: 'Verser à la GED'
        name: 'Versé à la GED'
        rule:
            last-action:
                - preparation-send-ged
                - error-ged
        action-automatique: orientation
        action-class: StandardAction
        connecteur-type: GED
        connecteur-type-action: GEDEnvoyer
        connecteur-type-mapping:
            fatal-error: error-ged
        editable-content:
            - envoi_sae
            - sae_show
            - journal
            - date_journal_debut
            - date_cloture_journal
            - date_cloture_journal_iso8601
            - sae_transfert_id
            - sae_bordereau
            - sae_archive
            - ar_sae
            - sae_ack_comment
            - reply_sae
            - sae_archival_identifier
            - sae_atr_comment
        modification-no-change-etat: true
    error-ged:
        name: 'Erreur irrécupérable lors du dépôt'
        rule:
            role_id_e: no-role
    preparation-send-sae:
        name: "Préparation de l'envoi au SAE"
        rule:
            role_id_e: no-role
        action-automatique: generate-sip
    generate-sip:
        name-action: "Générer le paquet d'archive (SIP)"
        name: "Paquet d'archive (SIP) généré"
        rule:
            last-action:
                - preparation-send-sae
                - generate-sip-error
                - rejet-sae
                - erreur-envoie-sae
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: Pastell\Step\SAE\Action\SAEGenerateArchiveAction
        action-automatique: send-archive
        connecteur-type-mapping:
            generate-sip-error: generate-sip-error
            sae_show: sae_show
            sae_bordereau: sae_bordereau
            sae_archive: sae_archive
            journal: journal
            date_journal_debut: date_journal_debut
            date_cloture_journal: date_cloture_journal
            date_cloture_journal_iso8601: date_cloture_journal_iso8601
    generate-sip-error:
        name: "Erreur lors de la génération de l'archive"
        rule:
            role_id_e: no-role
    send-archive:
        name-action: 'Verser au SAE'
        name: 'Versé au SAE'
        rule:
            last-action:
                - generate-sip
                - rejet-sae
                - erreur-envoie-sae
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: Pastell\Step\SAE\Action\SAESendArchiveAction
        action-automatique: verif-sae
        connecteur-type-mapping:
            sae_transfert_id: sae_transfert_id
            sae_bordereau: sae_bordereau
            sae_archive: sae_archive
            erreur-envoie-sae: erreur-envoie-sae
    erreur-envoie-sae:
        name: "Erreur lors de l'envoi au SAE"
        rule:
            role_id_e: no-role
    verif-sae:
        name-action: "Récupérer l'AR du dossier sur le SAE"
        name: "Récuperation de l'AR sur le SAE"
        rule:
            last-action:
                - send-archive
                - verif-sae-erreur
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: SAEVerifier
        connecteur-type-mapping:
            sae_transfert_id: sae_transfert_id
            ar_sae: ar_sae
            ar-recu-sae: ar-recu-sae
            verif-sae-erreur: verif-sae-erreur
            sae_ack_comment: sae_ack_comment
            sae_bordereau: sae_bordereau
            ack-not-provided: ack-not-provided
    verif-sae-erreur:
        name: "Erreur lors de la récupération de l'AR"
        rule:
            role_id_e: no-role
    ar-recu-sae:
        name: 'AR SAE reçu'
        rule:
            role_id_e: no-role
        action-automatique: validation-sae
    ack-not-provided:
        name: 'En attente de la validation par le SAE'
        rule:
            role_id_e: no-role
        action-automatique: validation-sae
    validation-sae:
        name-action: "Vérifier l'acceptation par le SAE"
        name: "Vérification de l'acceptation par le SAE"
        rule:
            last-action:
                - ar-recu-sae
                - ack-not-provided
                - validation-sae-erreur
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: SAEValider
        connecteur-type-mapping:
            sae_transfert_id: sae_transfert_id
            reply_sae: reply_sae
            erreur-envoie-sae: erreur-envoie-sae
            validation-sae-erreur: validation-sae-erreur
            accepter-sae: accepter-sae
            rejet-sae: rejet-sae
            sae_atr_comment: sae_atr_comment
            sae_archival_identifier: sae_archival_identifier
            sae_bordereau: sae_bordereau
    validation-sae-erreur:
        name: 'Erreur lors de la vérification de la validité du transfert'
        rule:
            role_id_e: no-role
    accepter-sae:
        name: 'Transfert accepté par le SAE'
        rule:
            role_id_e: no-role
        action-automatique: orientation
    rejet-sae:
        name: 'Transfert rejeté par le SAE'
        rule:
            role_id_e: no-role
restriction_pack:
    - suppl_recup_fin_parapheur
studio_definition: ewogICJpZF90eXBlX2Rvc3NpZXIiOiAiZHJhZnQtbHMtcmVjdXAtcGFyYXBoZXVyIiwKICAicGFzdGVsbC12ZXJzaW9uIjogIiVWRVJTSU9OJSIsCiAgInRpbWVzdGFtcCI6IDE2NzE2MzYzODYsCiAgInJhd19kYXRhIjogewogICAgImlkX3R5cGVfZG9zc2llciI6ICJkcmFmdC1scy1yZWN1cC1wYXJhcGhldXIiLAogICAgIm5vbSI6ICJSw6ljdXDDqXJhdGlvbiBwYXJhcGhldXIgKERyYWZ0KSIsCiAgICAidHlwZSI6ICJEb2N1bWVudHMgZGl2ZXJzIiwKICAgICJkZXNjcmlwdGlvbiI6ICJUeXBlIGRlIGRvc3NpZXIgcGVybWV0dGFudCBsYSBjb25uZXhpb24gXHUwMGUwIHVuIHBhcmFwaGV1ciBwb3VyIHJcdTAwZTljdXBcdTAwZTlyZXIgbGVzIGRvc3NpZXJzIiwKICAgICJub21fb25nbGV0IjogIkluZm9ybWF0aW9uIiwKICAgICJyZXN0cmljdGlvbl9wYWNrIjogIiIsCiAgICAiZm9ybXVsYWlyZUVsZW1lbnQiOiBbCiAgICAgIHsKICAgICAgICAiZWxlbWVudF9pZCI6ICJkb3NzaWVyX2lkIiwKICAgICAgICAibmFtZSI6ICJJZGVudGlmaWFudCBkdSBkb3NzaWVyIHN1ciBsZSBwYXJhcGhldXIiLAogICAgICAgICJ0eXBlIjogInRleHQiLAogICAgICAgICJjb21tZW50YWlyZSI6ICIiLAogICAgICAgICJyZXF1aXMiOiAib24iLAogICAgICAgICJjaGFtcHNfYWZmaWNoZXMiOiAib24iLAogICAgICAgICJjaGFtcHNfcmVjaGVyY2hlX2F2YW5jZWUiOiAib24iLAogICAgICAgICJ0aXRyZSI6IGZhbHNlLAogICAgICAgICJzZWxlY3RfdmFsdWUiOiAiIiwKICAgICAgICAicHJlZ19tYXRjaCI6ICIiLAogICAgICAgICJwcmVnX21hdGNoX2Vycm9yIjogIiIsCiAgICAgICAgImNvbnRlbnRfdHlwZSI6ICIiCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAiZWxlbWVudF9pZCI6ICJkb3NzaWVyX25hbWUiLAogICAgICAgICJuYW1lIjogIk5vbSBkdSBkb3NzaWVyIiwKICAgICAgICAidHlwZSI6ICJ0ZXh0IiwKICAgICAgICAiY29tbWVudGFpcmUiOiAiIiwKICAgICAgICAicmVxdWlzIjogIm9uIiwKICAgICAgICAiY2hhbXBzX2FmZmljaGVzIjogIiIsCiAgICAgICAgImNoYW1wc19yZWNoZXJjaGVfYXZhbmNlZSI6ICIiLAogICAgICAgICJ0aXRyZSI6ICJvbiIsCiAgICAgICAgInNlbGVjdF92YWx1ZSI6ICIiLAogICAgICAgICJwcmVnX21hdGNoIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2hfZXJyb3IiOiAiIiwKICAgICAgICAiY29udGVudF90eXBlIjogIiIKICAgICAgfSwKICAgICAgewogICAgICAgICJlbGVtZW50X2lkIjogImRvY3VtZW50X3NpZ25lIiwKICAgICAgICAibmFtZSI6ICJEb2N1bWVudCBzaWduXHUwMGU5IiwKICAgICAgICAidHlwZSI6ICJtdWx0aV9maWxlIiwKICAgICAgICAiY29tbWVudGFpcmUiOiAiIiwKICAgICAgICAicmVxdWlzIjogIm9uIiwKICAgICAgICAiY2hhbXBzX2FmZmljaGVzIjogIiIsCiAgICAgICAgImNoYW1wc19yZWNoZXJjaGVfYXZhbmNlZSI6ICIiLAogICAgICAgICJ0aXRyZSI6ICIiLAogICAgICAgICJzZWxlY3RfdmFsdWUiOiAiIiwKICAgICAgICAicHJlZ19tYXRjaCI6ICIiLAogICAgICAgICJwcmVnX21hdGNoX2Vycm9yIjogIiIsCiAgICAgICAgImNvbnRlbnRfdHlwZSI6ICIiCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAiZWxlbWVudF9pZCI6ICJhbm5leGUiLAogICAgICAgICJuYW1lIjogIkFubmV4ZSIsCiAgICAgICAgInR5cGUiOiAibXVsdGlfZmlsZSIsCiAgICAgICAgImNvbW1lbnRhaXJlIjogIiIsCiAgICAgICAgInJlcXVpcyI6ICIiLAogICAgICAgICJjaGFtcHNfYWZmaWNoZXMiOiAiIiwKICAgICAgICAiY2hhbXBzX3JlY2hlcmNoZV9hdmFuY2VlIjogIiIsCiAgICAgICAgInRpdHJlIjogIiIsCiAgICAgICAgInNlbGVjdF92YWx1ZSI6ICIiLAogICAgICAgICJwcmVnX21hdGNoIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2hfZXJyb3IiOiAiIiwKICAgICAgICAiY29udGVudF90eXBlIjogIiIKICAgICAgfSwKICAgICAgewogICAgICAgICJlbGVtZW50X2lkIjogImJvcmRlcmVhdSIsCiAgICAgICAgIm5hbWUiOiAiQm9yZGVyZWF1IiwKICAgICAgICAidHlwZSI6ICJmaWxlIiwKICAgICAgICAiY29tbWVudGFpcmUiOiAiIiwKICAgICAgICAicmVxdWlzIjogIm9uIiwKICAgICAgICAiY2hhbXBzX2FmZmljaGVzIjogIiIsCiAgICAgICAgImNoYW1wc19yZWNoZXJjaGVfYXZhbmNlZSI6ICIiLAogICAgICAgICJ0aXRyZSI6ICIiLAogICAgICAgICJzZWxlY3RfdmFsdWUiOiAiIiwKICAgICAgICAicHJlZ19tYXRjaCI6ICIiLAogICAgICAgICJwcmVnX21hdGNoX2Vycm9yIjogIiIsCiAgICAgICAgImNvbnRlbnRfdHlwZSI6ICIiCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAiZWxlbWVudF9pZCI6ICJwcmVtaXMiLAogICAgICAgICJuYW1lIjogIkZpY2hpZXIgUFJFTUlTIiwKICAgICAgICAidHlwZSI6ICJmaWxlIiwKICAgICAgICAiY29tbWVudGFpcmUiOiAiTGUgZmljaGllciBQUkVNSVMgY29udGllbnQgbGVzIG1cdTAwZTl0YWRvbm5cdTAwZTllcyBldCBsJ2hpc3RvcmlxdWUgZHUgcGFyYXBoZXVyIiwKICAgICAgICAicmVxdWlzIjogIm9uIiwKICAgICAgICAiY2hhbXBzX2FmZmljaGVzIjogIiIsCiAgICAgICAgImNoYW1wc19yZWNoZXJjaGVfYXZhbmNlZSI6ICIiLAogICAgICAgICJ0aXRyZSI6ICIiLAogICAgICAgICJzZWxlY3RfdmFsdWUiOiAiIiwKICAgICAgICAicHJlZ19tYXRjaCI6ICIiLAogICAgICAgICJwcmVnX21hdGNoX2Vycm9yIjogIiIsCiAgICAgICAgImNvbnRlbnRfdHlwZSI6ICIiCiAgICAgIH0KICAgIF0sCiAgICAiZXRhcGUiOiBbCiAgICAgIHsKICAgICAgICAibnVtX2V0YXBlIjogMCwKICAgICAgICAidHlwZSI6ICJ0cmFuc2Zvcm1hdGlvbiIsCiAgICAgICAgImxhYmVsIjogIiIsCiAgICAgICAgImRlZmF1bHRDaGVja2VkIjogZmFsc2UsCiAgICAgICAgInJlcXVpcyI6ICJvbiIsCiAgICAgICAgImF1dG9tYXRpcXVlIjogIm9uIiwKICAgICAgICAic3BlY2lmaWNfdHlwZV9pbmZvIjogW10sCiAgICAgICAgIm51bV9ldGFwZV9zYW1lX3R5cGUiOiAwLAogICAgICAgICJldGFwZV93aXRoX3NhbWVfdHlwZV9leGlzdHMiOiBmYWxzZQogICAgICB9LAogICAgICB7CiAgICAgICAgIm51bV9ldGFwZSI6IDEsCiAgICAgICAgInR5cGUiOiAiZGVwb3QiLAogICAgICAgICJsYWJlbCI6ICIiLAogICAgICAgICJkZWZhdWx0Q2hlY2tlZCI6IGZhbHNlLAogICAgICAgICJyZXF1aXMiOiAiIiwKICAgICAgICAiYXV0b21hdGlxdWUiOiAib24iLAogICAgICAgICJzcGVjaWZpY190eXBlX2luZm8iOiBbXSwKICAgICAgICAibnVtX2V0YXBlX3NhbWVfdHlwZSI6IDAsCiAgICAgICAgImV0YXBlX3dpdGhfc2FtZV90eXBlX2V4aXN0cyI6IGZhbHNlCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAibnVtX2V0YXBlIjogIjIiLAogICAgICAgICJ0eXBlIjogInNhZSIsCiAgICAgICAgImxhYmVsIjogbnVsbCwKICAgICAgICAiZGVmYXVsdENoZWNrZWQiOiBmYWxzZSwKICAgICAgICAicmVxdWlzIjogIiIsCiAgICAgICAgImF1dG9tYXRpcXVlIjogIm9uIiwKICAgICAgICAic3BlY2lmaWNfdHlwZV9pbmZvIjogewogICAgICAgICAgInNhZV9oYXNfbWV0YWRhdGFfaW5fanNvbiI6ICIiCiAgICAgICAgfSwKICAgICAgICAibnVtX2V0YXBlX3NhbWVfdHlwZSI6IDAsCiAgICAgICAgImV0YXBlX3dpdGhfc2FtZV90eXBlX2V4aXN0cyI6IGZhbHNlCiAgICAgIH0KICAgIF0KICB9Cn0K
