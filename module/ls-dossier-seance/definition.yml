nom: 'Dossier de séance (archivage)'
type: 'Actes administratifs'
description: "Type de dossier à utiliser pour l'archivage des dossiers de séance de la collectivité."
connecteur:
    - 'Bordereau SEDA'
    - SAE
affiche_one: false
formulaire:
    'Informations principales':
        dossier:
            name: 'Dossier de séance'
            type: file
            requis: true
            multiple: false
            commentaire: 'Déposer le fichier ZIP contenant les données de la séance'
            content-type: application/zip
            title: true
        metadonnees:
            name: 'Fichier de métadonnées'
            type: file
            requis: true
            multiple: false
            commentaire: ''
    Cheminement:
        envoi_sae:
            name: "Système d'archivage électronique"
            type: checkbox
            onchange: cheminement-change
            default: checked
            read-only: true
    SAE:
        sae_show:
            no-show: true
        journal:
            name: 'Faisceau de preuves'
            type: file
        date_journal_debut:
            name: 'Date du premier évènement journalisé'
        date_cloture_journal:
            name: 'Date de clôture du journal'
        date_cloture_journal_iso8601:
            name: 'Date de clôture du journal (iso 8601)'
        sae_transfert_id:
            name: 'Identifiant de transfert'
            index: true
        sae_bordereau:
            name: 'Bordereau SEDA'
            type: file
        sae_archive:
            name: "Paquet d'archive (SIP)"
            type: file
        ar_sae:
            type: file
            name: 'Accusé de réception SAE'
        sae_ack_comment:
            name: "Commentaire de l'accusé de réception"
        reply_sae:
            type: file
            name: 'Réponse du SAE'
        sae_archival_identifier:
            name: "Identifiant de l'archive sur le SAE"
        sae_atr_comment:
            name: 'Commentaire de la réponse du SAE'
champs-affiches:
    - titre
    - dernier_etat
    - date_dernier_etat
champs-recherche-avancee:
    - type
    - id_e
    - lastetat
    - last_state_begin
    - etatTransit
    - state_begin
    - notEtatTransit
    - search
page-condition:
    SAE:
        sae_show: true
action:
    creation:
        name-action: Créer
        name: Créé
        rule:
            no-last-action: ''
    modification:
        name-action: Modifier
        name: 'En cours de rédaction'
        rule:
            last-action:
                - creation
                - modification
                - importation
    supression:
        name-action: Supprimer
        name: Supprimé
        rule:
            last-action:
                - creation
                - modification
                - importation
                - termine
                - fatal-error
                - rejet-sae
        action-class: Supprimer
        warning: 'Êtes-vous sûr ?'
    importation:
        name: 'Importation du document'
        rule:
            role_id_e: no-role
        action-automatique: orientation
    orientation:
        name: 'Envoi du document'
        name-action: 'Envoyer le document'
        rule:
            last-action:
                - creation
                - modification
                - importation
                - accepter-sae
            document_is_valide: true
        action-class: OrientationTypeDossierPersonnalise
    termine:
        name: 'Traitement terminé'
        rule:
            role_id_e: no-role
    reouverture:
        name: 'Rouvrir le dossier'
        rule:
            last-action:
                - termine
        action-class: Reopen
    cheminement-change:
        no-workflow: true
        rule:
            role_id_e: 'no-role,'
        action-class: CheminementChangeTypeDossierPersonnalise
    preparation-send-sae:
        name: "Préparation de l'envoi au SAE"
        rule:
            role_id_e: no-role
        action-automatique: generate-sip
    generate-sip:
        name-action: "Générer le paquet d'archive (SIP)"
        name: "Paquet d'archive (SIP) généré"
        rule:
            last-action:
                - preparation-send-sae
                - generate-sip-error
                - rejet-sae
                - erreur-envoie-sae
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: Pastell\Step\SAE\Action\SAEGenerateArchiveAction
        action-automatique: send-archive
        connecteur-type-mapping:
            generate-sip-error: generate-sip-error
            sae_show: sae_show
            sae_bordereau: sae_bordereau
            sae_archive: sae_archive
            journal: journal
            date_journal_debut: date_journal_debut
            date_cloture_journal: date_cloture_journal
            date_cloture_journal_iso8601: date_cloture_journal_iso8601
    generate-sip-error:
        name: "Erreur lors de la génération de l'archive"
        rule:
            role_id_e: no-role
    send-archive:
        name-action: 'Verser au SAE'
        name: 'Versé au SAE'
        rule:
            last-action:
                - generate-sip
                - rejet-sae
                - erreur-envoie-sae
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: Pastell\Step\SAE\Action\SAESendArchiveAction
        action-automatique: verif-sae
        connecteur-type-mapping:
            sae_transfert_id: sae_transfert_id
            sae_bordereau: sae_bordereau
            sae_archive: sae_archive
            erreur-envoie-sae: erreur-envoie-sae
    erreur-envoie-sae:
        name: "Erreur lors de l'envoi au SAE"
        rule:
            role_id_e: no-role
    verif-sae:
        name-action: "Récupérer l'AR du dossier sur le SAE"
        name: "Récuperation de l'AR sur le SAE"
        rule:
            last-action:
                - send-archive
                - verif-sae-erreur
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: SAEVerifier
        connecteur-type-mapping:
            sae_transfert_id: sae_transfert_id
            ar_sae: ar_sae
            ar-recu-sae: ar-recu-sae
            verif-sae-erreur: verif-sae-erreur
            sae_ack_comment: sae_ack_comment
            sae_bordereau: sae_bordereau
            ack-not-provided: ack-not-provided
    verif-sae-erreur:
        name: "Erreur lors de la récupération de l'AR"
        rule:
            role_id_e: no-role
    ar-recu-sae:
        name: 'AR SAE reçu'
        rule:
            role_id_e: no-role
        action-automatique: validation-sae
    ack-not-provided:
        name: 'En attente de la validation par le SAE'
        rule:
            role_id_e: no-role
        action-automatique: validation-sae
    validation-sae:
        name-action: "Vérifier l'acceptation par le SAE"
        name: "Vérification de l'acceptation par le SAE"
        rule:
            last-action:
                - ar-recu-sae
                - ack-not-provided
                - validation-sae-erreur
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: SAEValider
        connecteur-type-mapping:
            sae_transfert_id: sae_transfert_id
            reply_sae: reply_sae
            erreur-envoie-sae: erreur-envoie-sae
            validation-sae-erreur: validation-sae-erreur
            accepter-sae: accepter-sae
            rejet-sae: rejet-sae
            sae_atr_comment: sae_atr_comment
            sae_archival_identifier: sae_archival_identifier
            sae_bordereau: sae_bordereau
    validation-sae-erreur:
        name: 'Erreur lors de la vérification de la validité du transfert'
        rule:
            role_id_e: no-role
    accepter-sae:
        name: 'Transfert accepté par le SAE'
        rule:
            role_id_e: no-role
        action-automatique: orientation
    rejet-sae:
        name: 'Transfert rejeté par le SAE'
        rule:
            role_id_e: no-role
studio_definition: ewogICJpZF90eXBlX2Rvc3NpZXIiOiAiZHJhZnQtbHMtZG9zc2llci1zZWFuY2UiLAogICJwYXN0ZWxsLXZlcnNpb24iOiAiJVZFUlNJT04lIiwKICAidGltZXN0YW1wIjogMTY5MTE1ODI1MywKICAicmF3X2RhdGEiOiB7CiAgICAiaWRfdHlwZV9kb3NzaWVyIjogImRyYWZ0LWxzLWRvc3NpZXItc2VhbmNlIiwKICAgICJub20iOiAiRG9zc2llciBkZSBzw6lhbmNlIChhcmNoaXZhZ2UpIChEcmFmdCkiLAogICAgInR5cGUiOiAiQWN0ZXMgYWRtaW5pc3RyYXRpZnMiLAogICAgImRlc2NyaXB0aW9uIjogIlR5cGUgZGUgZG9zc2llciBcdTAwZTAgdXRpbGlzZXIgcG91ciBsJ2FyY2hpdmFnZSBkZXMgZG9zc2llcnMgZGUgc1x1MDBlOWFuY2UgZGUgbGEgY29sbGVjdGl2aXRcdTAwZTkuIiwKICAgICJub21fb25nbGV0IjogIkluZm9ybWF0aW9ucyBwcmluY2lwYWxlcyIsCiAgICAicmVzdHJpY3Rpb25fcGFjayI6ICIiLAogICAgImZvcm11bGFpcmVFbGVtZW50IjogWwogICAgICB7CiAgICAgICAgImVsZW1lbnRfaWQiOiAiZG9zc2llciIsCiAgICAgICAgIm5hbWUiOiAiRG9zc2llciBkZSBzXHUwMGU5YW5jZSIsCiAgICAgICAgInR5cGUiOiAiZmlsZSIsCiAgICAgICAgImNvbW1lbnRhaXJlIjogIkRcdTAwZTlwb3NlciBsZSBmaWNoaWVyIFpJUCBjb250ZW5hbnQgbGVzIGRvbm5cdTAwZTllcyBkZSBsYSBzXHUwMGU5YW5jZSIsCiAgICAgICAgInJlcXVpcyI6ICJvbiIsCiAgICAgICAgImNoYW1wc19hZmZpY2hlcyI6ICIiLAogICAgICAgICJjaGFtcHNfcmVjaGVyY2hlX2F2YW5jZWUiOiAiIiwKICAgICAgICAidGl0cmUiOiAib24iLAogICAgICAgICJzZWxlY3RfdmFsdWUiOiAiIiwKICAgICAgICAicHJlZ19tYXRjaCI6ICIiLAogICAgICAgICJkZWZhdWx0X3ZhbHVlIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2hfZXJyb3IiOiAiIiwKICAgICAgICAiY29udGVudF90eXBlIjogImFwcGxpY2F0aW9uXC96aXAiCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAiZWxlbWVudF9pZCI6ICJtZXRhZG9ubmVlcyIsCiAgICAgICAgIm5hbWUiOiAiRmljaGllciBkZSBtXHUwMGU5dGFkb25uXHUwMGU5ZXMiLAogICAgICAgICJ0eXBlIjogImZpbGUiLAogICAgICAgICJjb21tZW50YWlyZSI6ICIiLAogICAgICAgICJyZXF1aXMiOiAib24iLAogICAgICAgICJjaGFtcHNfYWZmaWNoZXMiOiAiIiwKICAgICAgICAiY2hhbXBzX3JlY2hlcmNoZV9hdmFuY2VlIjogIiIsCiAgICAgICAgInRpdHJlIjogZmFsc2UsCiAgICAgICAgInNlbGVjdF92YWx1ZSI6ICIiLAogICAgICAgICJwcmVnX21hdGNoIjogIiIsCiAgICAgICAgImRlZmF1bHRfdmFsdWUiOiAiIiwKICAgICAgICAicHJlZ19tYXRjaF9lcnJvciI6ICIiLAogICAgICAgICJjb250ZW50X3R5cGUiOiAiIgogICAgICB9CiAgICBdLAogICAgImV0YXBlIjogWwogICAgICB7CiAgICAgICAgIm51bV9ldGFwZSI6IDAsCiAgICAgICAgInR5cGUiOiAic2FlIiwKICAgICAgICAibGFiZWwiOiBudWxsLAogICAgICAgICJkZWZhdWx0Q2hlY2tlZCI6IGZhbHNlLAogICAgICAgICJyZXF1aXMiOiAib24iLAogICAgICAgICJhdXRvbWF0aXF1ZSI6ICJvbiIsCiAgICAgICAgInNwZWNpZmljX3R5cGVfaW5mbyI6IHsKICAgICAgICAgICJzYWVfaGFzX21ldGFkYXRhX2luX2pzb24iOiAiIgogICAgICAgIH0sCiAgICAgICAgIm51bV9ldGFwZV9zYW1lX3R5cGUiOiAwLAogICAgICAgICJldGFwZV93aXRoX3NhbWVfdHlwZV9leGlzdHMiOiBmYWxzZQogICAgICB9CiAgICBdCiAgfQp9
