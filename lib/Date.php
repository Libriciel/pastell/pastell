<?php

class Date
{
    public const DATE_ISO = "Y-m-d H:i:s";
    public const DATE_FR = "d/m/Y H:i:s";
}
