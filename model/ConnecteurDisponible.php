<?php

use Pastell\Service\Droit\DroitService;
use Pastell\Service\Entite\EntityUtilitiesService;

class ConnecteurDisponible
{
    public const DROIT_NEDEED = 'connecteur:edition';

    public function __construct(
        private readonly EntiteSQL $entiteSQL,
        private readonly RoleUtilisateur $roleUtilisateur,
        private readonly ConnecteurEntiteSQL $connecteurEntiteSQL,
        private readonly DroitService $droitService,
        private readonly EntityUtilitiesService $entityUtilitiesService,
    ) {
    }

    /** @deprecated Since 4.1.6, Unused, Use getListByType instead */
    public function getList(int $id_u, int $id_e, string $type): array
    {
        return $this->getListByType($id_u, $id_e, $type);
    }

    /**
     * Liste des connecteurs disponibles de type globaux ou d'entité pour id_e avec les droits de id_u
     */
    public function getListByType(int $id_u, int $id_e, string $type, bool $global = false): array
    {
        $ancetre = $this->entiteSQL->getAncetreId($id_e);
        if ($id_e === 0) {
            array_shift($ancetre);
        }
        $ancetre[] = $id_e;
        $ancetre = array_reverse($ancetre);
        $result = [];

        foreach ($ancetre as $entite_id_e) {
            if ($this->roleUtilisateur->hasDroit($id_u, self::DROIT_NEDEED, $entite_id_e)) {
                $listDisponible = $this->entityUtilitiesService->addDenominationForEntiteRacine(
                    $this->connecteurEntiteSQL->getDisponible($entite_id_e, $type, $global)
                );
                $result = array_merge($result, $listDisponible);
            }
        }
        return $this->droitService->clearRestrictedConnecteur($result);
    }
}
