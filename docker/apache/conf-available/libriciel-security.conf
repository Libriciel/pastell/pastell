# Reducing MIME type security risks
Header set X-Content-Type-Options "nosniff"

# HTTP Strict Transport Security (HSTS)
Header always set Strict-Transport-Security "max-age=63072000; includeSubDomains"

# Clickjacking
Header set X-Frame-Options "DENY"
<FilesMatch "\.(appcache|atom|bbaw|bmp|crx|css|cur|eot|f4[abpv]|flv|geojson|gif|htc|ico|jpe?g|js|json(ld)?|m4[av]|manifest|map|mp4|oex|og[agv]|opus|otf|pdf|png|rdf|rss|safariextz|svgz?|swf|topojson|tt[cf]|txt|vcard|vcf|vtt|webapp|web[mp]|webmanifest|woff2?|xloc|xml|xpi)$">
Header unset X-Frame-Options
</FilesMatch>
# Reflected Cross-Site Scripting (XSS) attacks
Header set X-XSS-Protection "1; mode=block"
 <FilesMatch "\.(appcache|atom|bbaw|bmp|crx|css|cur|eot|f4[abpv]|flv|geojson|gif|htc|ico|jpe?g|js|json(ld)?|m4[av]|manifest|map|mp4|oex|og[agv]|opus|otf|pdf|png|rdf|rss|safariextz|svgz?|swf|topojson|tt[cf]|txt|vcard|vcf|vtt|webapp|web[mp]|webmanifest|woff2?|xloc|xml|xpi)$">
Header unset X-XSS-Protection
</FilesMatch>
# Server software information
Header unset X-Powered-By

# On ne peut pas activer TLSv1.3, ca ne fonctionne pas bien avec l'authentification par certificat
SSLProtocol -all -TLSv1.3 +TLSv1.2

SSLCipherSuite          ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
SSLHonorCipherOrder     off
SSLSessionTickets       off

SSLUseStapling On
SSLStaplingCache "shmcb:logs/ssl_stapling(32768)"

Header set Content-Security-Policy "frame-ancestors 'self';"
Header set Referrer-Policy: same-origin
Header set Permissions-Policy "accelerometer=(), geolocation=('self'), fullscreen=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(), display-capture=('self')"
