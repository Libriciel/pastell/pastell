<?php

declare(strict_types=1);

use Pastell\Connector\AbstractSedaGeneratorConnector;
use Pastell\Seda\Message\SedaMessageBuilder;
use Pastell\Tests\Connector\AbstractSedaGeneratorConnectorTestCase;

final class SedaGeneratorAsalae10Test extends AbstractSedaGeneratorConnectorTestCase
{
    public function getSedaMessageBuilder(): SedaMessageBuilder
    {
        return new SedaMessageBuilder(
            $this->getTmpFolder(),
            $this->getPastellMetadataService()
        );
    }

    public function getSedaConnectorId(): string
    {
        return 'generateur-seda-asalae-1.0';
    }

    public function getExpectedCallDirectory(): string
    {
        return __DIR__ . '/seda-test-cases';
    }

    public function testGenerateEmptyArchive(): void
    {
        $id_ce = $this->createSedaGeneriqueConnector();
        /** @var AbstractSedaGeneratorConnector $sedaGeneriqueConnector */
        $sedaGeneriqueConnector = $this->getConnecteurFactory()->getConnecteurById($id_ce);
        $this->expectException(UnrecoverableException::class);
        $this->expectExceptionMessage('Aucun fichier à archiver');
        $sedaGeneriqueConnector->generateArchive(new \FluxDataTest([]), 'empty');
    }
}
