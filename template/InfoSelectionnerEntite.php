<?php

if (isset($show_choice_entity_message) && $show_choice_entity_message) : ?>
    <br/><br/>
    <div id="title-choose" class="alert alert-info">
      Veuillez sélectionner une entité afin de pouvoir visualiser ses dossiers.
    </div>
<?php endif; ?>
